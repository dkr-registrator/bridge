package bridge

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewError(t *testing.T) {
	bridge, err := New(nil, "", Config{})
	assert.Nil(t, bridge)
	assert.Error(t, err)
}

func TestNewValid(t *testing.T) {
	Register(new(fakeFactory), "consul")
	// Note: the following is valid for New() since it does not
	// actually connect to docker.
	bridge, err := New(nil, "consul://jjj:8080", Config{})

	assert.NotNil(t, bridge)
	assert.NoError(t, err)
}
