package bridge

import (
	"log"
	"net/netip"
	"strings"
)

// Added for define network priority (Added from Nicolas HYPOLITE patch)
func (b *Bridge) ipDefine(service *Service, metadata map[string]string) (ip string, finded bool) {
	// Get configuration from registrator himselft
	net_prority := []string{}
	net_prority = b.config.NetworksPriority

	// Check if container ha custom network priority label
	if priority, exist := b.networksPriorityContainerRules(metadata); exist {
		net_prority = priority
	}

	for _, netw := range net_prority {
		parsedNet := netip.Prefix{}
		var err error

		// If string container dot, it s an IP
		isIP := strings.Contains(netw, ".")
		if isIP {
			parsedNet, err = netip.ParsePrefix(netw)
			if err != nil {
				log.Println(err)
				continue
			}
		}

		for networkName, netContainer := range service.Networks {
			if isIP {
				ip, err := netip.ParseAddr(netContainer.IPAddress)
				if err != nil {
					continue
				}

				// If container IP is in current range (parsedNet), return IP
				if parsedNet.Contains(ip) {
					return ip.String(), true
				}
			} else if networkName == netw {
				if netInfo, ok := service.Networks[netw]; ok {
					return netInfo.IPAddress, true
				}
			}
		}

	}
	return "", false
}

func (b *Bridge) networksPriorityContainerRules(metadata map[string]string) (networks_priority []string, configExist bool) {
	if net, ok := metadata["networks_priority"]; ok {
		return strings.Split(net, ","), true
	}
	return []string{}, false
}
