package bridge

import (
	"path"
	"strings"
)

func (b *Bridge) defineServiceName(port ServicePort) string {

	container := port.container
	var defaultName string

	// Path SERVICE_NAME = sha256 on new Docker version
	if name, ok := container.Config.Labels["com.docker.swarm.service.name"]; ok {
		defaultName = strings.Split(name, ".")[0]
	} else if imgName := strings.Split(path.Base(container.Image), ":")[0]; imgName != "sha256" {
		defaultName = strings.Split(path.Base(container.Image), ":")[0]
	} else {
		withoutTag := strings.Split(container.Config.Image, ":")[0]
		defaultName = strings.Split(withoutTag, "/")[len(strings.Split(withoutTag, "/"))-1]
	}

	return defaultName
}
