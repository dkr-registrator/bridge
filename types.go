//go:generate go-extpoints . AdapterFactory
package bridge

import (
	"net/url"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/network"
)

type AdapterFactory interface {
	New(uri *url.URL) RegistryAdapter
}

type RegistryAdapter interface {
	Ping() error
	Register(service *Service) error
	Deregister(service *Service) error
	Refresh(service *Service) error
	Services() ([]*Service, error)
}

type Config struct {
	HostIp           string
	Internal         bool
	Explicit         bool
	UseIpFromLabel   string
	ForceTags        string
	RefreshTtl       int
	RefreshInterval  int
	DeregisterCheck  string
	Cleanup          bool
	NetworksPriority []string
}

type Service struct {
	ID   string
	Name string
	Port int
	// Added for define network priority (Added from Nicolas HYPOLITE patch)
	// Store network name and IPAM
	Networks map[string]*network.EndpointSettings
	IP       string
	Tags     []string
	Attrs    map[string]string
	TTL      int

	Origin ServicePort
}

type DeadContainer struct {
	TTL      int
	Services []*Service
}

type ServicePort struct {
	HostPort          string
	HostIP            string
	ExposedPort       string
	ExposedIP         string
	PortType          string
	ContainerHostname string
	ContainerID       string
	ContainerName     string
	container         types.ContainerJSON
}

// func test() {
// 	tt := ServicePort{}
// 	tt.container.HostConfig.NetworkMode
// }
